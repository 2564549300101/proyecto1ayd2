# Tarea #1 Comandos git

## Integrantes
1. Irving Samuel Rosales Dominguez
   - 201403904
2. Kevin Alexander García Jachac
   - 201709155
3. Gabriela Elizabeth Zavala López
   - 201701131 
4. Oscar René Jordán Orellana
   - 201122846
5. Jose Fernando Cabrera García
	- 201020955

## CREAR REPOSITORIOS - Integrante #1

#### $ git init [project-name]
##### Crea un nuevo repositorio local con el nombre especificado
	`
	Ejemplo: git init Proyecto1AyD2 
	`
#### $ git clone [url]
##### Descarga un proyecto y toda su historia de versión
	`
	Ejemplo: git clone https://gitlab.com/2564549300101/proyecto1ayd2.git 
	`


## RESETEAR ARCHIVO - Integrante #2
	
#### $ git reset [file]
##### Mueve el archivo del área de espera, pero preserva su contenido
	`
	Ejemplo: git reset ./archivo.ts


## EFECTUAR CAMBIOS - Integrante #3

#### $ git add [file]
##### Toma una instantánea del archivo para preparar la versión
	`
	Ejemplo: git add ./nuevoindex.ts

## CAMBIOS GRUPALES - Integrante #4

#### $ git branch [branch-name]
##### Crea una nueva rama

	Ejemplo: git branch rama

## GIT DIFF - Integrante #5

#### $ git diff [file]
##### Ver las diferencias que hay entre el archivo local y el archivo remoto
	`
	Ejemplo: $ git diff README.md



